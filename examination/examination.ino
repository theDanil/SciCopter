#include "DataControl.h"
#include "TinyGPS.h"
#include "DallasTemperature.h"
#include "OneWire.h"
#include "DHT.h"
#include <SD.h>
#include <SPI.h>
#include <SoftwareSerial.h>

#define TEMPPIN 9
#define DHTPin 2
#define LED 10
#define GREEN 11
#define RED 12
#define BLUE 13
#define DELAY 1000    // trough such miliseconds repeat data-write
#define RX 3
#define TX 4
#define GpsBaudrate 9600

TinyGPSPlus gps;
DHT dht(DHTPin, DHT11);
OneWire ds(TEMPPIN);
DallasTemperature temp(&ds);
File myfile;
SoftwareSerial gpsSerial(RX, TX);
DataControl data("data03.txt", &temp, &dht);
bool fileError = false;
unsigned long t;

String info() {
  String result = "";
  if (gps.location.isValid()) {
    result += String(gps.location.lat()) + " ";
    result += String(gps.location.lng()) + " ";
  }
  else {
    result += "inv ";
  }
  if (gps.date.isValid()) {
    if (gps.date.day() < 10) result += "0";
    result += String(gps.date.day()) + "/";
    if (gps.date.month() < 10) result += "0";
    result += String(gps.date.month()) + "/";
    result += String(gps.date.year()) + " ";
  }
  else {
    result += "inv ";
  }
  if (gps.time.isValid()) {
    if (gps.time.hour() < 10) result += "0";
    result += String(gps.time.hour()) + ":";
    if (gps.time.minute() < 10) result += "0";
    result += String(gps.time.minute()) + ":";
    if (gps.time.second() < 10) result += "0";
    result += String(gps.time.second()) + " ";
  }
  else {
    result += "inv ";
  }
  return result;
}

void general() {

  while (gpsSerial.available() > 0) {
    if (gps.encode(gpsSerial.read())) {
      data.writeCurrentData(info());
    }
    if (millis() > 5000 && gps.charsProcessed() < 10)
    {
      Serial.println(F("No GPS detected: check wiring."));
      while (true);
    }
  }
}


DeviceAddress addr;
void setup() {

  pinMode(10, OUTPUT);
  Serial.begin(115200);
  gpsSerial.begin(GpsBaudrate);
  fileError = not data.initialize();
  pinMode(LED, OUTPUT);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(BLUE, OUTPUT);
  digitalWrite(LED, HIGH);


}

void loop() {

  if (!fileError) {
    general();
  }
  else {
    Serial.println("file error");
  }

}


