#ifndef DATAWRITE_H
#define DATAWRITE_H

#include "Arduino.h"
#include "DallasTemperature.h"
#include "DHT.h"
#include <SPI.h>
#include <SD.h>

class DataControl{

  private:
    DeviceAddress addr;
    File myfile;
    String filename;
    DallasTemperature* temp;
    DHT* dht;
    boolean write_(String str);

  public:
    DataControl(String filename_, DallasTemperature* temp_, DHT* dht_);
    boolean initialize();
    boolean writeCurrentData(String extraInfo);
  
};



#endif
