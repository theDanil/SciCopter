#include "DataControl.h"

DataControl::DataControl(String filename_, DallasTemperature* temp_, DHT* dht_) {
  this->filename = filename_;
  this->temp = temp_;
  this->dht = dht_;
  temp->getAddress(addr, 0);
  temp->setResolution(addr, 9);
}

boolean DataControl::initialize() {
  temp->begin();
  //dht->begin();
  return SD.begin(10);
}

boolean DataControl::write_(String str) {
  this->myfile = SD.open(filename, FILE_WRITE);
  if (myfile) {
    myfile.print(str);
    myfile.close();
  }
  else {
    Serial.println("error open");
    return false;
  }
  return true;
}

//float readTemp(int j){
//  ds.reset();
//  ds.select(owAddr[j]);
//  ds.write(0x44,1);
//  
//  delay(1000);
//  
//  int present=ds.reset();
//  ds.select(owAddr[j]);
//  ds.write(0xBE);
//  
//  for(int i=0; i<9; i++) owData[j][i]=ds.read(); 
//
//  int16_t raw=(owData[j][1]<<8)|owData[j][0];
//  byte cfg=(owData[j][4]&0x60);
//  if(cfg==0x00) raw=raw&~7;
//  else if(cfg==0x20) raw=raw&~3;
//  else if(cfg==0x40) raw=raw&~1;
//  return float(raw)/16.0;

boolean DataControl::writeCurrentData(String extraInfo) {
  // write: 
  // extraInfo temperature humidity height\n
  temp->requestTemperatures();
  String t = String(temp->getTempC(addr));
  String h = "01";
  String height = String(1.0);
  String buff = extraInfo + " " + t + " " + h + " " + height + "\n";
  Serial.print(buff);
  return write_(buff);
}

