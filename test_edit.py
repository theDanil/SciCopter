with open('test1.txt', 'r') as file:
    data = file.readlines()
with open('test1.txt', 'w') as file:
    for i, line in enumerate(data):
        line = line.split()
        line[-1] = str(i)
        line[-2] = str(38)
        if i >= 200:
            line[-3] = str(-1.75)
        elif i >= 190:
            line[-3] = str(-2)
        elif i >= 170:
            line[-3] = str(-2.125)
        elif i >= 150:
            line[-3] = str(-2.25)
        elif i >= 130:
            line[-3] = str(-2.375)
        elif i >= 100:
            line[-3] = str(-2.5)
        elif i >= 80:
            line[-3] = str(-2.625)
        elif i >= 40:
            line[-3] = str(-2.75)
        elif i >= 30:
            line[-3] = str(-2.875)
        elif i >= 0:
            line[-3] = str(-3) 
        file.write(' '.join(line) + "\n")