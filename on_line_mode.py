from PyQt5 import Qt
import pyqtgraph as pg
import sys
from urllib.request import urlopen


class OnlineApp(Qt.QMainWindow):

    order_data = ['altitude', 'temperature', 'humidity']
    altitude = []
    temperature = []
    humidity = []
    max_range = 400

    def __init__(self):
        super().__init__()
        self.init_ui()
        self.init_menu()

    def init_ui(self):
        self.main_widget = Qt.QWidget()
        self.setCentralWidget(self.main_widget)
        self.layout = Qt.QVBoxLayout(self.main_widget)

        self.inf_label = Qt.QLabel()

        self.setWindowTitle("SciCopter")
        self.pg_widget = pg.PlotWidget()
        self.t_curve = self.pg_widget.plot()
        self.h_curve = self.pg_widget.plot()
        self.layout.addWidget(self.pg_widget)

        self.begin_btn = Qt.QPushButton("Begin")
        self.begin_btn.clicked.connect(self.begin)
        grid = Qt.QGridLayout()
        grid.setColumnStretch(0, 2)
        grid.addWidget(self.begin_btn, 0, 3)
        self.layout.addLayout(grid)

    def init_menu(self):
        pass

    def begin(self):
        self.loop_timer = Qt.QTimer()
        self.loop_timer.start(500)
        self.loop_timer.timeout.connect(self.loop)

    def loop(self):
        raw_data = urlopen("192.168.240.1/data/get/value")
        a, t, h = raw_data[-3:]
        self.altitude.append(a)
        self.humidity.append(h)
        self.temperature.append(t)
        self.t_curve.setData(self.altitude, self.temperature)
        self.h_curve.setData(self.altitude, self.humidity)


def main():
    app = Qt.QApplication([])
    oa = OnlineApp()
    oa.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
